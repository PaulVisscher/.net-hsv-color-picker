﻿/**
 * Copyright (c) 2012 Inès Duits and Paul Visscher
 * 
 * See the file License.txt for copying permission.
 */
namespace PickColorPanel
{
    class doubleBufferedPanel : System.Windows.Forms.Panel
    {
        public doubleBufferedPanel()
        {
            DoubleBuffered = true;
        }
    }
    partial class ColorPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.HuePicker = new doubleBufferedPanel();
            this.Color1 = new doubleBufferedPanel();
            this.Color2 = new doubleBufferedPanel();
            this.ShadePicker = new doubleBufferedPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ShadePicker, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(150, 150);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.42857F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.Controls.Add(this.HuePicker, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Color1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.Color2, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 127);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(150, 23);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // HuePicker
            // 
            this.HuePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HuePicker.Location = new System.Drawing.Point(1, 1);
            this.HuePicker.Margin = new System.Windows.Forms.Padding(1);
            this.HuePicker.Name = "HuePicker";
            this.HuePicker.Size = new System.Drawing.Size(105, 21);
            this.HuePicker.TabIndex = 0;
            this.HuePicker.Paint += new System.Windows.Forms.PaintEventHandler(this.HuePicker_Paint);
            this.HuePicker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HuePicker_MouseDown);
            this.HuePicker.MouseMove += new System.Windows.Forms.MouseEventHandler(this.HuePicker_MouseMove);
            this.HuePicker.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HuePicker_MouseUp);
            // 
            // Color1
            // 
            this.Color1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Color1.Location = new System.Drawing.Point(108, 1);
            this.Color1.Margin = new System.Windows.Forms.Padding(1);
            this.Color1.Name = "Color1";
            this.Color1.Size = new System.Drawing.Size(19, 21);
            this.Color1.TabIndex = 1;
            this.Color1.Paint += new System.Windows.Forms.PaintEventHandler(this.Color1_Paint);
            // 
            // Color2
            // 
            this.Color2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Color2.Location = new System.Drawing.Point(129, 1);
            this.Color2.Margin = new System.Windows.Forms.Padding(1);
            this.Color2.Name = "Color2";
            this.Color2.Size = new System.Drawing.Size(20, 21);
            this.Color2.TabIndex = 2;
            this.Color2.Paint += new System.Windows.Forms.PaintEventHandler(this.Color2_Paint);
            // 
            // ShadePicker
            // 
            this.ShadePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShadePicker.Location = new System.Drawing.Point(1, 1);
            this.ShadePicker.Margin = new System.Windows.Forms.Padding(1);
            this.ShadePicker.Name = "ShadePicker";
            this.ShadePicker.Size = new System.Drawing.Size(148, 125);
            this.ShadePicker.TabIndex = 1;
            this.ShadePicker.Paint += new System.Windows.Forms.PaintEventHandler(this.ShadePicker_Paint);
            this.ShadePicker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ShadePicker_MouseDown);
            this.ShadePicker.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ShadePicker_MouseMove);
            this.ShadePicker.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ShadePicker_MouseUp);
            // 
            // ColorPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "ColorPicker";
            this.Resize += new System.EventHandler(this.ShadePicker_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private doubleBufferedPanel HuePicker;
        private doubleBufferedPanel Color1;
        private doubleBufferedPanel Color2;
        private doubleBufferedPanel ShadePicker;
    }
}
