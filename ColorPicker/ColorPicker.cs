﻿/**
 * Copyright (c) 2012 Inès Duits and Paul Visscher
 * 
 * See the file License.txt for copying permission.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PickColorPanel
{
    /// <summary>
    /// A HSV color picker class
    /// </summary>
    public partial class ColorPicker : UserControl
    {
        /// <summary>
        /// Holds the primary selected color and 
        /// the secondary selected color.
        /// </summary>
        private Color pColor, sColor;
        /// <summary>
        /// Holds a list with the drawing points
        /// used by the gradient path brush.
        /// </summary>
        private PointF[] PointListPicker;
        /// <summary>
        /// The currently selected hue
        /// Hue holds the color information
        /// and has a value on the interval [0,360]
        /// </summary>
        private int hue = 0;
        /// <summary>
        /// The scale of the x positition of the
        /// hue selector bar
        /// </summary>
        private double HueSelectorScale = 0;
        /// <summary>
        /// Holds the dragging state for each
        /// form element
        /// </summary>
        private bool mouseDraggingHue = false, mouseDraggingShade = false;

        /// <summary>
        /// Color picker constructor
        /// </summary>
        public ColorPicker()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;

            //Set default colors
            this.setPrimaryColor("Black");
            this.setSecondaryColor("White");

            //scale the drawing
            this.ScalePicker();
        }

        /// <summary>
        /// Gets/sets the primary color
        /// either by name or by color object
        /// </summary>
        public Color PrimaryColor
        {
            get
            {
                return this.pColor;
            }
            set
            {
                this.setPrimaryColor(value);
            }
        }
        /// <summary>
        /// Gets/sets the secondary color
        /// either by name or by color object
        /// </summary>
        public Color SecondaryColor
        {
            get
            {
                return this.sColor;
            }
            set
            {
                this.setSecondaryColor(value);
            }
        }

        /// <summary>
        /// Set the primary color
        /// </summary>
        /// <param name="c">The color</param>
        private void setPrimaryColor(Color c)
        {
            this.pColor = c;
            this.Redraw();
        }
        /// <summary>
        /// Set the primary color
        /// </summary>
        /// <param name="name">Color name</param>
        private void setPrimaryColor(String name)
        {
            this.setPrimaryColor(Color.FromName(name));
        }
        /// <summary>
        /// Set the secondary color
        /// </summary>
        /// <param name="c">Color</param>
        private void setSecondaryColor(Color c)
        {
            this.sColor = c;
            this.Redraw();
        }
        /// <summary>
        /// Set the secondary color
        /// </summary>
        /// <param name="name">Color name</param>
        private void setSecondaryColor(String name)
        {
            this.setSecondaryColor(Color.FromName(name));
        }
        /// <summary>
        /// Scale the gradient points to the correct
        /// positions, so we can correctly draw the 
        /// gradient.
        /// </summary>
        private void ScalePicker()
        {
            this.PointListPicker = new PointF[4];
            this.PointListPicker[0] = new Point(0,0);
            this.PointListPicker[1] = new Point(this.ShadePicker.Width, 0);
            this.PointListPicker[2] = new Point(this.ShadePicker.Width, this.ShadePicker.Height);
            this.PointListPicker[3] = new Point(0, this.ShadePicker.Height);

            //force a rederaw
            this.Redraw(true);
        }
        /// <summary>
        /// Redraw the color picker element
        /// </summary>
        /// <param name="changedHue">Force a full redraw</param>
        private void Redraw(bool changedHue = false)
        {
            if (changedHue)
            {
                this.ShadePicker.Invalidate();
                this.HuePicker.Invalidate();
            }
            this.Color1.Invalidate();
            this.Color2.Invalidate();
        }
        /// <summary>
        /// Give a hsv color and convert it to rgb
        /// </summary>
        /// <param name="hue">The hue [0,360]</param>
        /// <param name="saturation">The saturatio [0,1]</param>
        /// <param name="value">The value [0,1]</param>
        /// <returns>The color</returns>
        private Color HSVtoRGB(float hue, float saturation, float value)
        {
            double hdiv60 = hue / 60.0d;
            double h60smallestZ = Math.Floor(hdiv60);

            int hi = (int)(h60smallestZ) % 6;

            double f = hdiv60 - h60smallestZ;
            double p = value * (1 - saturation);
            double q = value * (1 - f * saturation);
            double t = value * (1 - (1 - f) * saturation);

            double r = 0, g = 0, b = 0;

            switch (hi)
            {
                case 0:
                    r = value; g = t; b = p;
                    break;
                case 1:
                    r = q; g = value; b = p;
                    break;
                case 2:
                    r = p; g = value; b = t;
                    break;
                case 3:
                    r = p; g = q; b = value;
                    break;
                case 4:
                    r = t; g = p; b = value;
                    break;
                case 5:
                    r = value; g = p; b = q;
                    break;
            }

            r = r * 255;
            g = g * 255;
            b = b * 255;

            return Color.FromArgb((int)r, (int)g, (int)b);
        }
        /// <summary>
        /// Keeps a given nummer between a given interval
        /// </summary>
        /// <param name="n">The number</param>
        /// <param name="min">The minimal value</param>
        /// <param name="max">The maximal value</param>
        /// <returns>The number on the interval</returns>
        private double MinMaxInterval(double n, double min, double max)
        {
            if (n < min)
                n = min;
            else if (n > max)
                n = max;
            return n;
        }
        /// <summary>
        /// Keeps a given nummer between a given interval
        /// </summary>
        /// <param name="n">The number</param>
        /// <param name="min">The minimal value</param>
        /// <param name="max">The maximal value</param>
        /// <returns>The number on the interval</returns>
        private int intMinMaxInterval(int n, int min, int max)
        {
            return (int)this.MinMaxInterval(n,min,max);
        }

        #region events
        /// <summary>
        /// Color 1 box paint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Color1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(this.pColor),0,0,this.Color1.Width,this.Color1.Height);
        }
        /// <summary>
        /// Color 2 box paint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Color2_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(this.sColor), 0, 0, this.Color2.Width, this.Color2.Height);
        }
        /// <summary>
        /// Hue picker paint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HuePicker_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rec = new Rectangle(0, 0, this.HuePicker.Width, this.HuePicker.Height);
            
            LinearGradientBrush grad = new LinearGradientBrush(rec, HSVtoRGB(0f, 1f, 1f), HSVtoRGB(360f, 1f, 1f), LinearGradientMode.Horizontal);

            ColorBlend cb = new ColorBlend();

            List<float> posF = new List<float>();
            List<Color> colorL = new List<Color>();

            // add each color
            for (int i = 0; i <= 360;i+=15)
            {
                posF.Add(i/360f);
                colorL.Add(HSVtoRGB(i,1f,1f));
            }

            cb.Positions = posF.ToArray();
            cb.Colors = colorL.ToArray();

            grad.InterpolationColors = cb;

            e.Graphics.FillRectangle(grad,rec);
            //selection bar
            e.Graphics.FillRectangle(Brushes.Black, (int)(HueSelectorScale * this.HuePicker.Width)-1, 0,2,this.Height);
        }
        /// <summary>
        /// Shade picker paint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShadePicker_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            if (this.PointListPicker != null)
            {
                PathGradientBrush pBrush = new PathGradientBrush(this.PointListPicker);

                //set the correct colors
                Color[] colors = {
                    Color.FromArgb(255, this.HSVtoRGB(this.hue,0f,1f)),
                    Color.FromArgb(255, this.HSVtoRGB(this.hue,1f,1f)),
                    Color.FromArgb(255, this.HSVtoRGB(this.hue,0f,0f)),
                    Color.FromArgb(255, this.HSVtoRGB(this.hue,1f,0f)),
                                 };

                pBrush.SurroundColors = colors;
                pBrush.CenterColor = HSVtoRGB(this.hue, 0.5f, 0.5f);

                e.Graphics.FillRectangle(pBrush, new Rectangle(0, 0, this.ShadePicker.Width, this.ShadePicker.Height));
            }
        }
        /// <summary>
        /// Hue picker mouse down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HuePicker_MouseDown(object sender, MouseEventArgs e)
        {
            //hue =  mouse location x / hue picker width * 360
            HueSelectorScale = (double)e.X / (double)this.HuePicker.Width;
            HueSelectorScale = MinMaxInterval(HueSelectorScale,0,1);

            this.hue = (int)(HueSelectorScale * 360);
            this.Redraw(true);

            mouseDraggingHue = true;
        }
        /// <summary>
        /// Hue picker mouseup event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HuePicker_MouseUp(object sender, MouseEventArgs e)
        {
            this.ResetDragging();
        }
        /// <summary>
        /// Hue picker mouse move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HuePicker_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDraggingHue)
                HuePicker_MouseDown(sender, e);
        }
        /// <summary>
        /// Shade picker mousedown event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShadePicker_MouseDown(object sender, MouseEventArgs e)
        { 
            //saturation = mouse location x / shadepicker width
            //value = 1 - mouse location y / shadepicker height
            Point p = new Point();
            p.X = intMinMaxInterval(e.X, 0, this.ShadePicker.Width);
            p.Y = intMinMaxInterval(e.Y, 0, this.ShadePicker.Height);

            double scaleX = (double)p.X / this.ShadePicker.Width;
            double scaleY = (double)p.Y / this.ShadePicker.Height;
            Color c = Color.FromArgb(255, this.HSVtoRGB((float)this.hue, (float)(scaleX), (float)(1 - scaleY)));

            //set the correct color
            if (e.Button == MouseButtons.Left)
            {
                this.setPrimaryColor(c);
            }
            else
            {
                this.setSecondaryColor(c);
            }

            mouseDraggingShade = true;
        }
        /// <summary>
        /// Shade picker mouse move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShadePicker_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDraggingShade)
                ShadePicker_MouseDown(sender, e);
        }
        /// <summary>
        /// Shade picker mouse up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShadePicker_MouseUp(object sender, MouseEventArgs e)
        {
            this.ResetDragging();
        }
        /// <summary>
        /// Resets the dragging variables
        /// </summary>
        private void ResetDragging()
        {
            mouseDraggingShade = false;
            mouseDraggingHue = false;
        }
        /// <summary>
        /// ShadePicker resize event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShadePicker_Resize(object sender, EventArgs e)
        {
            //scale the gradient
            this.ScalePicker();
        }
        #endregion
    }
}
