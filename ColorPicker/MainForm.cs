﻿/**
 * Copyright (c) 2012 Inès Duits and Paul Visscher
 * 
 * See the file License.txt for copying permission.
 */
using System.Windows.Forms;

namespace PickColorPanel
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.Controls.Add(new ColorPicker());
        }
    }
}
