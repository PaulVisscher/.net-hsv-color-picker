﻿/**
 * Copyright (c) 2012 Inès Duits and Paul Visscher
 * 
 * See the file License.txt for copying permission.
 */
using System;
using System.Windows.Forms;

namespace PickColorPanel
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
